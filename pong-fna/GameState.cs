using System;
namespace pongfna
{
	public enum GameState
	{
		START,
		PLAY,
		PAUSE,
		GAME_OVER
	}

	public class MaintainGameState
	{
		private GameState GameState;

		public MaintainGameState(GameState gameState)
		{
			GameState = gameState;
		}

		public GameState GetCurrentState()
		{
			return GameState;
		}

		public void UpdateState(GameState gameState)
		{
			GameState = gameState;
		}
	}
}
