using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using pongfna;
using pongfna.entities;

namespace pong
{
	class PongFNAGame : Game
	{
		[Flags]
		public enum Alignment { Center = 0, Left = 1, Right = 2, Top = 4, Bottom = 8 }

		GraphicsDeviceManager graphics;
		Paddle player;
		Paddle aiPlayer;
		Ball ball;
		Wall bottom;
		Wall top;
		Wall left;
		MaintainGameState gameState = new MaintainGameState(GameState.START);

		SpriteFont font;
		bool play = false;
		int ScreenWidth = 0;
		int ScreenHeight = 0;

		string winningPlayer = "";
		Color DefaultObjectColor;

		Stopwatch stopWatch;

		float scaleX;
		float scaleY;
		Matrix matrix;

		int VirtualWidth = 1280;
		int VirtualHeight = 720;

		public PongFNAGame()
		{
			graphics = new GraphicsDeviceManager(this);
			graphics.PreferredBackBufferWidth = 1280;
			graphics.PreferredBackBufferHeight = 720;
			graphics.PreferMultiSampling = true;
			Content.RootDirectory = "Content";

			Window.AllowUserResizing = false;
			IsMouseVisible = false;

			DefaultObjectColor = Color.White;
			stopWatch = new Stopwatch();
		}


		SpriteBatch spriteBatch;

		protected override void LoadContent()
		{
			spriteBatch = new SpriteBatch(GraphicsDevice);
			font = Content.Load<SpriteFont>("arcade_classic");

			Texture2D paddleTex = Content.Load<Texture2D>("paddle");
			player = new Paddle(paddleTex, GraphicsDevice.Viewport, Keys.Up, Keys.Down);
			aiPlayer = new Paddle(paddleTex, GraphicsDevice.Viewport, Keys.W, Keys.S, false);
			ball = new Ball(Content.Load<Texture2D>("ball"), GraphicsDevice.Viewport);

			bottom = new Wall(paddleTex, GraphicsDevice.Viewport, new Point(0, GraphicsDevice.Viewport.Height));
			top = new Wall(paddleTex, GraphicsDevice.Viewport, new Point(0, 15));
			left = new Wall(paddleTex, GraphicsDevice.Viewport, new Point(15, 0));

			ScreenWidth = GraphicsDevice.Viewport.Width;
			ScreenHeight = GraphicsDevice.Viewport.Height;

			scaleX = (float)ScreenWidth / VirtualWidth;
			scaleY = (float)ScreenHeight / VirtualHeight;
			matrix = Matrix.CreateScale(scaleX, scaleY, 1.0f);
			base.LoadContent();
		}

		protected override void UnloadContent()
		{
			Content.Unload();

			spriteBatch.Dispose();

			base.UnloadContent();
		}



		protected override void Update(GameTime gameTime)
		{
			Input.Update(IsActive);

			//
			// Asset Rebuilding:
#if DEBUG
			if(Input.KeyWentDown(Keys.F5))
			{
				if(AssetRebuild.Run())
				{
					UnloadContent();
					LoadContent();
				}
			}
			if (stopWatch.IsRunning && stopWatch.ElapsedMilliseconds >= 2000)
			{
				stopWatch.Stop();
				play = true;
			}
#endif
			if (gameState.GetCurrentState() == GameState.PLAY && play)
			{
				if (ball.Collides(player) || ball.Collides(aiPlayer))
				{
					float dx = -ball.Velocity.X * 1.03f;
					if (ball.Velocity.Y < 0)
					{
						ball.Velocity = new Vector2(dx, -150f);
					}
					else
					{
						ball.Velocity = new Vector2(dx, 150f);
					}
				}
				if (ball.Position.X < player.Position.X)
				{
					aiPlayer.Score += 1;
					Reset();
				}
				else if (ball.Position.X > aiPlayer.Position.X)
				{
					player.Score += 1;
					Reset();
				}

				if (player.Score + aiPlayer.Score == 10)
				{
					gameState.UpdateState(GameState.GAME_OVER);
					if (player.Score > aiPlayer.Score)
					{
						winningPlayer = "Left";
					}
					else if (player.Score < aiPlayer.Score)
					{
						winningPlayer = "Right";
					}
					else
					{
						winningPlayer = "No player";
					}
				}
				ball.Update(gameTime);
				player.Update(gameTime);
				aiPlayer.Update(gameTime);
			}
			if (Input.KeyWentDown(Keys.Enter) && (gameState.GetCurrentState() == GameState.PAUSE ||
				gameState.GetCurrentState() == GameState.START))
			{
				gameState.UpdateState(GameState.PLAY);
				stopWatch.Restart();
			} else if (Input.KeyWentDown(Keys.Enter) && gameState.GetCurrentState() == GameState.PLAY)
			{
				gameState.UpdateState(GameState.PAUSE);
				play = false;
			} else if (Input.KeyWentDown(Keys.Enter) && gameState.GetCurrentState() == GameState.GAME_OVER)
			{
				Reset();
				winningPlayer = "";
				player.Score = 0;
				aiPlayer.Score = 0;
				gameState.UpdateState(GameState.PLAY);
			}
			base.Update(gameTime);
		}


		protected override void Draw(GameTime gameTime)
		{
			//
			// Replace this with your own drawing code.
			//

			GraphicsDevice.Clear(Color.Black);

			spriteBatch.Begin(SpriteSortMode.Deferred,
				BlendState.AlphaBlend,
				SamplerState.LinearClamp,
				DepthStencilState.None,
				RasterizerState.CullCounterClockwise,
				null,
				matrix);

			player.Draw(spriteBatch);
			aiPlayer.Draw(spriteBatch);
			if (gameState.GetCurrentState() == GameState.PLAY)
			{
				ball.Draw(spriteBatch);
			}
			bottom.Draw(spriteBatch);
			top.Draw(spriteBatch);
			
			if (gameState.GetCurrentState() == GameState.START)
			{
				DrawString(font, new StringBuilder("Pong"), new Rectangle(ScreenWidth / 2, ScreenHeight / 2, 0, 0), Alignment.Center, DefaultObjectColor);
				DrawString(font, new StringBuilder("Press Enter to Start"), new Rectangle(ScreenWidth / 2, ScreenHeight / 2 + 50, 0, 0), Alignment.Center, DefaultObjectColor);
			}
			if (gameState.GetCurrentState() == GameState.PAUSE)
			{
				DrawString(font, new StringBuilder("Paused"), new Rectangle(ScreenWidth / 2, ScreenHeight / 2, 0, 0), Alignment.Center, DefaultObjectColor);
			}
			if (gameState.GetCurrentState() == GameState.PLAY)
			{
				if (play == false && stopWatch.IsRunning)
				{
					int time = (int)Math.Ceiling((double)stopWatch.ElapsedMilliseconds / 1000);
					DrawString(font, new StringBuilder(time.ToString()), new Rectangle(ScreenWidth / 2, ScreenHeight / 2, 0, 0), Alignment.Bottom, DefaultObjectColor);
				}
				DrawString(font, new StringBuilder(player.Score.ToString()), new Rectangle(ScreenWidth / 2 - 150, ScreenHeight / 3, 0, 0), Alignment.Center, DefaultObjectColor);
				DrawString(font, new StringBuilder(aiPlayer.Score.ToString()), new Rectangle(ScreenWidth / 2 + 130, ScreenHeight / 3, 0, 0), Alignment.Center, DefaultObjectColor);
			}
			if (gameState.GetCurrentState() == GameState.GAME_OVER)
			{
				DrawString(font, new StringBuilder(winningPlayer + " Won!"), new Rectangle(ScreenWidth / 2, ScreenHeight / 2, 0, 0), Alignment.Center, DefaultObjectColor);
				DrawString(font, new StringBuilder("Press Enter to Restart"), new Rectangle(ScreenWidth / 2, ScreenHeight / 2 + 50, 0, 0), Alignment.Center, DefaultObjectColor);
			}
			spriteBatch.Draw(left.Image, new Rectangle(0, 0, 15, ScreenHeight), DefaultObjectColor);
			spriteBatch.Draw(left.Image, new Rectangle(ScreenWidth - 15, 0, 15, ScreenHeight), DefaultObjectColor);
			spriteBatch.End();

			base.Draw(gameTime);
		}

		public void DrawString(SpriteFont font, StringBuilder text, Rectangle bounds, Alignment align, Color color)
		{
			Vector2 size = font.MeasureString(text);
			Vector2 pos = new Vector2(bounds.Center.X, bounds.Center.Y);
			Vector2 origin = size * 0.5f;

			if (align.HasFlag(Alignment.Left))
				origin.X += bounds.Width / 2 - size.X / 2;

			if (align.HasFlag(Alignment.Right))
				origin.X -= bounds.Width / 2 - size.X / 2;

			if (align.HasFlag(Alignment.Top))
				origin.Y += bounds.Height / 2 - size.Y / 2;

			if (align.HasFlag(Alignment.Bottom))
				origin.Y -= bounds.Height / 2 - size.Y / 2;

			spriteBatch.DrawString(font, text, pos, color, 0, origin, 2, SpriteEffects.None, 0);
		}

		private void Reset()
		{
			stopWatch.Restart();
			ball.Reset();
			player.Reset();
			aiPlayer.Reset();
			play = false;
		}

	}
}
