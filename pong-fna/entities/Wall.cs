using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace pongfna.entities
{
	public class Wall : BaseEntity
	{
		public Wall(Texture2D image, Viewport viewport, Point location) : base(image)
		{
			ScreenWidth = viewport.Width;
			ScreenHeight = viewport.Height;
			Dimensions = new Point(ScreenWidth, 15);
			Position = new Vector2(location.X, location.Y - Dimensions.Y);
		}

		public override void Update(GameTime gameTime)
		{
			// TODO: Any updates?
		}
	}
}
