using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using pong;

namespace pongfna.entities
{
	public class Paddle : BaseEntity
	{
		private Keys UpKey;
		private Keys DownKey;

		public int Score { get; set; }
		private Vector2 OldPosition;

		public Paddle(Texture2D image, Viewport viewport, Keys up, Keys down, bool isLeft = true) : base(image)
		{
			Dimensions = new Point(20, 150);
			ScreenWidth = viewport.Width;
			ScreenHeight = viewport.Height;
			if (!isLeft)
			{
				Position = new Vector2(7 * ScreenWidth / 8 - Dimensions.X, ScreenHeight / 4);
			} else
			{
				Position = new Vector2(ScreenWidth / 8 - Dimensions.X, ScreenHeight / 4);
			}
			OldPosition = Position;
			
			UpKey = up;
			DownKey = down;
			Score = 0;
		}

		public override void Update(GameTime gameTime)
		{
			float yChange = HandleInput();
			if (yChange + Dimensions.Y >= ScreenHeight)
			{
				yChange = ScreenHeight - Dimensions.Y;
			}
			if (yChange <= 0)
			{
				yChange = 0;
			}
			Position = new Vector2(Position.X, yChange);
		}

		// For overriding in case of AI
		public virtual float HandleInput()
		{
			float yChange = Position.Y;
			if (Input.KeyWentDown(UpKey) ||
				Input.IsKeyDown(UpKey))
			{
				yChange -= 8f;
			}
			if (Input.KeyWentDown(DownKey) ||
				Input.IsKeyDown(DownKey))
			{
				yChange += 8f;
			}
			return yChange;
		}

		public void Reset()
		{
			Position = OldPosition;
		}
	}
}
