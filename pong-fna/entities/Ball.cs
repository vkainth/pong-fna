using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace pongfna.entities
{
	public class Ball : BaseEntity
	{
		private Random random;
		public Ball(Texture2D image, Viewport viewport) : base(image)
		{
			Dimensions = new Point(30, 30);
			ScreenHeight = viewport.Height;
			ScreenWidth = viewport.Width;
			random = new Random();
			Reset();
		}

		public override void Update(GameTime gameTime)
		{
			if (Velocity.LengthSquared() < 2000)
			{
				Velocity += Acceleration;
			}
			BoundaryCheck();
			Position += (Velocity * (float)gameTime.ElapsedGameTime.TotalSeconds);
			
		}

		private void BoundaryCheck()
		{
			// Replace X checks with collision check for behind player, more consistent
			if ((Position.Y + Dimensions.Y >= ScreenHeight) ||
				Position.Y <= 0)
			{
				Velocity = new Vector2(Velocity.X, -Velocity.Y);
			}
		}

		public void Reset()
		{
			Position = new Vector2(ScreenWidth / 2, ScreenHeight / 2);
			int randomNumber = random.Next(0, 1);
			if (randomNumber == 0)
			{
				Acceleration = new Vector2(-300f, 300f);
			} else
			{
				Acceleration = new Vector2(300f, 300f);
			}
			
			Velocity = new Vector2(0, 0);
		}

		public bool Collides(BaseEntity entity)
		{
			if (Position.X > entity.Position.X + entity.Dimensions.X ||
				entity.Position.X > Position.X + Dimensions.X)
			{
				return false;
			}
			if (Position.Y > entity.Position.Y + entity.Dimensions.Y ||
				entity.Position.Y > Position.Y + Dimensions.Y)
			{
				return false;
			}
			return true;
		}
	}
}
