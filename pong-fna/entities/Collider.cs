using System;
using Microsoft.Xna.Framework;

namespace pongfna.entities
{
	public class Collider
	{
		public static bool IsColliding(Rectangle A, Rectangle B)
		{
			return A.Intersects(B) || A.Contains(B);
		}
	}
}
