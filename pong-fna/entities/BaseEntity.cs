using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace pongfna.entities
{
	abstract public class BaseEntity
	{
		public Vector2 Position { get; set; }
		public Vector2 Velocity { get; set; }
		public Vector2 Acceleration { get; set; }
		public int ScreenHeight { get; set; }
		public int ScreenWidth { get; set; }

		public Point Dimensions { get; set; }

		public Texture2D Image { get; set; }

		public Color Color { get; set; }

		public BaseEntity(Texture2D image)
		{
			Image = image;
			Position = new Vector2(100, 100);
			Velocity = new Vector2(0, 0);
			Acceleration = new Vector2(0, 0);
			Dimensions = new Point(16, 16);
			Color = Color.White;
			ScreenHeight = 720;
			ScreenWidth = 1280;
		}
		virtual public void Draw(SpriteBatch batch)
		{
			batch.Draw(Image, new Rectangle((int)Position.X, (int)Position.Y, Dimensions.X, Dimensions.Y), Color);
		}

		abstract public void Update(GameTime gameTime);
	}
}
